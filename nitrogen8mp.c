// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2019 NXP
 * Copyright 2020 Boundary Devices
 */

#include <common.h>
#include <command.h>
#include <asm/arch/clock.h>
#include <asm/arch/imx8mp_pins.h>
#include <asm/arch/sys_proto.h>
#include <asm/io.h>
#include <asm/mach-imx/dma.h>
#include <asm/mach-imx/fbpanel.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <asm-generic/gpio.h>

#include <display_detect.h>
#include <dwc3-uboot.h>
#include <errno.h>
#include <linux/delay.h>
#include <miiphy.h>
#include <mmc.h>
#include <netdev.h>
#include <power/pmic.h>
#include <spl.h>
#include <usb.h>
#include "../common/padctrl.h"
#include "../common/bd_common.h"

DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_FSEL1)
#define WDOG_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_ODE | PAD_CTL_PUE | PAD_CTL_PE)

static iomux_v3_cfg_t const init_pads[] = {
	IOMUX_PAD_CTRL(GPIO1_IO01__GPIO1_IO01, 0x100),
	IOMUX_PAD_CTRL(GPIO1_IO02__WDOG1_WDOG_B, WDOG_PAD_CTRL),
	IOMUX_PAD_CTRL(UART2_RXD__UART2_DCE_RX, UART_PAD_CTRL),
	IOMUX_PAD_CTRL(UART2_TXD__UART2_DCE_TX, UART_PAD_CTRL),
	IOMUX_PAD_CTRL(SAI1_RXD3__ENET1_MDIO, PAD_CTRL_ENET_MDIO),
	IOMUX_PAD_CTRL(SAI1_RXD2__ENET1_MDC, PAD_CTRL_ENET_MDC),

	/* eqos */
#define GP_EQOS_MII_MDC		IMX_GPIO_NR(1, 16)
	IOMUX_PAD_CTRL(ENET_MDC__ENET_QOS_MDC, 0x3),
#define GP_EQOS_MII_MDIO	IMX_GPIO_NR(1, 17)
	IOMUX_PAD_CTRL(ENET_MDIO__ENET_QOS_MDIO, 0x3),
	/* fec */
#define GP_FEC_MII_MDC		IMX_GPIO_NR(4, 4)
	IOMUX_PAD_CTRL(SAI1_RXD2__ENET1_MDC, 0x3),
#define GP_FEC_MII_MDIO		IMX_GPIO_NR(4, 5)
	IOMUX_PAD_CTRL(SAI1_RXD3__ENET1_MDIO, 0x3),

#define GP_USB3_1_HUB_RESET	IMX_GPIO_NR(1, 10)
	IOMUX_PAD_CTRL(GPIO1_IO10__GPIO1_IO10, WEAK_PULLUP_OUTPUT),
	//IOMUX_PAD_CTRL(GPIO1_IO12__GPIO1_IO12, 0x100),
	//IOMUX_PAD_CTRL(GPIO1_IO13__HSIOMIX_usb1_OTG_OC,	0x116),
	//IOMUX_PAD_CTRL(SAI1_RXD0__GPIO4_IO02, 0x00),	/* float for OTG1 mode */
#define GP_FASTBOOT_KEY		IMX_GPIO_NR(5, 9)
	IOMUX_PAD_CTRL(ECSPI1_SS0__GPIO5_IO09, WEAK_PULLUP),
	IOMUX_PAD_CTRL(ECSPI1_MISO__GPIO5_IO08, WEAK_PULLDN_OUTPUT),
};

int board_early_init_f(void)
{
	struct wdog_regs *wdog = (struct wdog_regs *)WDOG1_BASE_ADDR;

	imx_iomux_v3_setup_multiple_pads(init_pads, ARRAY_SIZE(init_pads));
	set_wdog_reset(wdog);
	init_uart_clk(1);

	return 0;
}

#ifdef CONFIG_CMD_FBPANEL
#ifdef CONFIG_VIDEO_IMX8MP_HDMI
int board_detect_hdmi(struct display_info_t const *di)
{
	int ret =  display_detect_by_node_name("hdmi_hpd");

	return (ret > 0) ? 1 : 0;
}
#endif

static const struct display_info_t displays[] = {
	/* hdmi */
#ifdef CONFIG_VIDEO_IMX8MP_HDMI
	VD_1920_1080M_60(HDMI, board_detect_hdmi, 0, 0x50),
#else
	VD_1920_1080M_60(HDMI, NULL, 0, 0x50),
#endif
	VD_1280_800M_60(HDMI, NULL, 0, 0x50),
	VD_1280_720M_60(HDMI, NULL, 0, 0x50),
	VD_1024_768M_60(HDMI, NULL, 0, 0x50),
	VD_640_480M_60(HDMI, NULL, 0, 0x50),
};
#define display_cnt	ARRAY_SIZE(displays)
#else
#define displays	NULL
#define display_cnt	0
#endif

int board_init(void)
{
	gpio_request(GP_EQOS_MII_MDC, "eqos_mdc");
	gpio_request(GP_EQOS_MII_MDIO, "eqos_mdio");
	gpio_request(GP_FEC_MII_MDC, "fec_mdc");
	gpio_request(GP_FEC_MII_MDIO, "fec_mdio");
	gpio_request(GP_USB3_1_HUB_RESET, "usb1_hub_reset");

	gpio_direction_output(GP_USB3_1_HUB_RESET, 0);
  gpio_set_value(GP_USB3_1_HUB_RESET, 0);
  mdelay(10);
  gpio_set_value(GP_USB3_1_HUB_RESET, 1);
  gpio_free(GP_USB3_1_HUB_RESET);

#ifdef CONFIG_DM_ETH
	board_eth_init(gd->bd);
#endif
#ifdef CONFIG_CMD_FBPANEL
	fbp_setup_display(displays, display_cnt);
#endif
	return 0;
}

#if defined(CONFIG_CMD_FASTBOOT) || defined(CONFIG_CMD_DFU)
int board_fastboot_key_pressed(void)
{
	gpio_request(GP_FASTBOOT_KEY, "fastboot_key");
	gpio_direction_input(GP_FASTBOOT_KEY);
	return !gpio_get_value(GP_FASTBOOT_KEY);
}
#endif

static int board_carrier = -1;
static int board_rv = -1;

static void check_board_env(void)
{
	if (board_rv == 20)
		env_set("board_rv", "_r20");
#ifdef CONFIG_FEC_MXC	/* bm has only 1 phy as well */
	if (board_carrier >= 0)
		env_set("board_carrier", board_carrier ? "-enc" : NULL);
#endif
}

void board_eth_type(int index, int ksz)
{
	if (index == 0) {
		board_rv = ksz ? 20 : 0;
		if (board_carrier < 0)
			board_carrier = 1;
	} else if (index == 1) {
		board_carrier = 0;	/* EVK */
	}
	check_board_env();
}

void board_env_init(void)
{
	check_board_env();
}
